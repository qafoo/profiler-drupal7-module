# Drupal Module for Qafoo Profiler

Download the module into `sites/all/module/qafooprofiler` and activate it.

Update index.php with the following code if you have the Daemon installed:

    require_once DRUPAL_ROOT . '/sites/all/modules/qafooprofiler/profiler.php';
    \QafooLabs\Profiler::start('<api key here>');

And the following code if you are only profiling locally in development:

    require_once DRUPAL_ROOT . '/sites/all/modules/qafooprofiler/profiler.php';
    \QafooLabs\Profiler::startDevelopment('<api key here>');

This is required to profile the complete Drupal request and not skip parts of
the boostrapping, which can be quite slow itself.
